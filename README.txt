Pentru a rula aplicatia trebuie sa urmati urmatorii pasi:

-In primul rand trebuie sa aveti instalat Android Studio pe sistemul dumneavoastra
-In cazul in care nu il aveti instalat, urmati tutorialul de instalare: https://www.youtube.com/watch?v=u0jM8QjMeVM

-Daca aveti deja Android Studio:
-> Descarcati codul sursa de pe Bitbucket
->Deschideti Android Studio -> File -> Open -> calea_catre_proiectul_descarcat
->In cazul in care aveti erori la rulare intrati in folder-ul unde aveti proiectul -> app folder -> stergeti folderul de build(se va crea altul automat la urmatorul build)
