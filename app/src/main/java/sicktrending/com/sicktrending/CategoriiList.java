package sicktrending.com.sicktrending;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.andremion.counterfab.CounterFab;
import com.firebase.ui.database.FirebaseRecyclerAdapter;
import com.firebase.ui.database.FirebaseRecyclerOptions;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.Query;
import com.squareup.picasso.Picasso;

import io.paperdb.Paper;
import sicktrending.com.sicktrending.Common.Common;
import sicktrending.com.sicktrending.Database.Database;
import sicktrending.com.sicktrending.Interface.ItemClickListener;
import sicktrending.com.sicktrending.Model.CategoriiPiese;
import sicktrending.com.sicktrending.ViewHolder.CategoriiViewHolder;

import uk.co.chrisjenx.calligraphy.CalligraphyConfig;
import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;


public class CategoriiList extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener {

    RecyclerView recyclerView;
    RecyclerView.LayoutManager layoutManager;

    FirebaseDatabase database;
    DatabaseReference categoriiList;
    String categoryId = "";
    FirebaseRecyclerAdapter<CategoriiPiese, CategoriiViewHolder> adapter;
    SwipeRefreshLayout swipeRefreshLayout;
    CounterFab fab1;
    TextView txtFullName;



    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase));
    }


    @SuppressLint("ResourceAsColor")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        CalligraphyConfig.initDefault(new CalligraphyConfig.Builder()
                .setDefaultFontPath("font/caviar.ttf")
                .setFontAttrId(R.attr.fontPath)
                .build());
        setContentView(R.layout.activity_categorii);

        //Firebase
        database = FirebaseDatabase.getInstance();
        categoriiList = database.getReference("CategoriiPiese");
        swipeRefreshLayout = (SwipeRefreshLayout) findViewById(R.id.swipe_layout);
        swipeRefreshLayout.setColorSchemeColors(R.color.colorPrimary,
                android.R.color.holo_blue_dark,
                android.R.color.holo_orange_dark,
                android.R.color.holo_red_dark
        );
        swipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                //Get Intent here
                if (getIntent() != null)
                    categoryId = getIntent().getStringExtra("CategoryId");
                if (!categoryId.isEmpty() && categoryId != null) {
                    if (Common.isConnectedToInternet(getBaseContext()))
                        loadCategorii(categoryId);
                    else {
                        Toast.makeText(CategoriiList.this, "No internet connection.", Toast.LENGTH_SHORT).show();
                        return;
                    }
                }
            }
        });
        swipeRefreshLayout.post(new Runnable() {
            @Override
            public void run() {
                //Get Intent here
                if (getIntent() != null)
                    categoryId = getIntent().getStringExtra("CategoryId");
                if (!categoryId.isEmpty() && categoryId != null) {
                    if (Common.isConnectedToInternet(getBaseContext()))
                        loadCategorii(categoryId);
                    else {
                        Toast.makeText(CategoriiList.this, "No internet connection.", Toast.LENGTH_SHORT).show();
                        return;
                    }
                }


            }
        });


        recyclerView = (RecyclerView) findViewById(R.id.recycler_categorii);
        recyclerView.setHasFixedSize(true);
        //layoutManager = new LinearLayoutManager(this);
        //recycler_piese.setLayoutManager(layoutManager);
        recyclerView.setLayoutManager(new GridLayoutManager(this, 1));

        fab1 = (CounterFab) findViewById(R.id.fab1);
        fab1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent cartIntent = new Intent(CategoriiList.this,Cart.class);
                startActivity(cartIntent);
            }
        });
        fab1.setCount(new Database(this).getCountCart());

        NavigationView navigationView = (NavigationView) findViewById(R.id.navigation_view);
        navigationView.setNavigationItemSelectedListener(this);

        //Set Name for user
        View headerView = navigationView.getHeaderView(0);
        txtFullName = (TextView)headerView.findViewById(R.id.txtFullName);
        txtFullName.setText(Common.currentUser.getLastName()+ " "+ Common.currentUser.getFirstName());

    }

    @Override
    protected void onResume() {
        super.onResume();
        fab1.setCount(new Database(this).getCountCart());
        if (adapter != null)
            adapter.startListening();
    }

    private void loadCategorii(String categoryId) {

        Query searchByName = categoriiList.orderByChild("CategoryId").equalTo(categoryId);

        FirebaseRecyclerOptions<CategoriiPiese> categoriiOptions = new FirebaseRecyclerOptions.Builder<CategoriiPiese>()
                .setQuery(searchByName, CategoriiPiese.class)
                .build();

        adapter = new FirebaseRecyclerAdapter<CategoriiPiese, CategoriiViewHolder>(categoriiOptions) {
            @Override
            protected void onBindViewHolder(@NonNull CategoriiViewHolder viewHolder, int position, @NonNull CategoriiPiese model) {

                viewHolder.txtCategorieName.setText(model.getName());
                Picasso.with(getBaseContext()).load(model.getImage()).into(viewHolder.imageView);
                final CategoriiPiese clickItem = model;
                viewHolder.setItemClickListener(new ItemClickListener() {
                    @Override
                    public void onClick(View view, int position, boolean isLongClick) {
                        //Get CategoryId and send to new Activity
                        Intent pieseList = new Intent(CategoriiList.this, PieseList.class);
                        pieseList.putExtra("PieseId", adapter.getRef(position).getKey());
                        startActivity(pieseList);
                    }
                });
            }

            @Override
            public CategoriiViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
                View itemView = LayoutInflater.from(parent.getContext())
                        .inflate(R.layout.categorii_item, parent, false);
                return new CategoriiViewHolder(itemView);
            }
        };

        adapter.startListening();

        recyclerView.setAdapter(adapter); //adapter for Recycler View is Search result
        swipeRefreshLayout.setRefreshing(false);

    }

    @Override
    protected void onStop() {
        super.onStop();
        adapter.stopListening();
    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_category);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }



    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        int id = item.getItemId();

        if (id == R.id.nav_cart) {
            Intent cartIntent = new Intent(CategoriiList.this, Cart.class);
            startActivity(cartIntent);
            onBackPressed();
        }else if(id == R.id.nav_home){
            Intent homeIntent = new Intent(CategoriiList.this,Home.class);
            startActivity(homeIntent);
            onBackPressed();
            }
        else if (id == R.id.nav_log_out) {

            //Delete Remember user & pass
            Paper.book().destroy();

            Intent signIn = new Intent(CategoriiList.this, MainActivity.class);
            signIn.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
            startActivity(signIn);

        }
        else if(id == R.id.nav_about_us){
            CalligraphyConfig.initDefault(new CalligraphyConfig.Builder()
                    .setDefaultFontPath("font/caviar.ttf")
                    .setFontAttrId(R.attr.fontPath)
                    .build());
            AlertDialog.Builder alertDialog = new AlertDialog.Builder(CategoriiList.this);
            alertDialog.setTitle("Despre noi");
            alertDialog.setMessage("Ne gasiti pe Strada Mihai Viteazul 42, Brasov 500187\n" +
                    "Telefon: 031 130 0429\n" + "Email: vanzari@carparts.com\n");

            alertDialog.setNegativeButton("OK", new DialogInterface.OnClickListener() {

                @Override
                public void onClick(DialogInterface dialogInterface, int i) {
                    dialogInterface.dismiss();
                }
            });

            alertDialog.show();

        }
        else if (id == R.id.nav_user_settings){
            Intent userSettingsIntent = new Intent(CategoriiList.this,UserSettings.class);
            startActivity(userSettingsIntent);

        }

        return true;
    }


}