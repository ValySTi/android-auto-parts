package sicktrending.com.sicktrending.Model;

public class CategoriiPiese {
    private String Name;
    private  String Image;

    public CategoriiPiese() {
    }

    public CategoriiPiese(String name, String image) {
        Name = name;
        Image = image;
    }

    public String getName() {
        return Name;
    }

    public void setName(String name) {
        Name = name;
    }

    public String getImage() {
        return Image;
    }

    public void setImage(String image) {
        Image = image;
    }
}


