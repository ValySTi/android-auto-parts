package sicktrending.com.sicktrending.Model;

import java.util.List;



public class Request {
    private String phone;
    private String name;
    private String address;
    private String total;
    private String paymentState;
    private List<Order> piese;

    public Request() {
    }

    public Request(String phone, String name, String address, String total, String paymentState, List<Order> piese) {
        this.phone = phone;
        this.name = name;
        this.address = address;
        this.total = total;

        this.paymentState = paymentState;
        this.piese = piese;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getTotal() {
        return total;
    }

    public void setTotal(String total) {
        this.total = total;
    }

    public String getPaymentState() {
        return paymentState;
    }

    public void setPaymentState(String paymentState) {
        this.paymentState = paymentState;
    }

    public List<Order> getPiese() {
        return piese;
    }

    public void setPiese(List<Order> piese) {
        this.piese = piese;
    }
}
