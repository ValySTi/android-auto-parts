package sicktrending.com.sicktrending.Model;



public class User {

    private String firstName;
    private String lastName;
    private String email;
    private String phone;
    private String password;

    //Constructors
    public User() {};

    public User(String firstName1, String lastName,String email,String phone,String password) {
        this.firstName = firstName1;
        this.lastName = lastName;
        this.email = email;
        this.phone = phone;
        this.password = password;

    }

    public User(String firstName1, String lastName,String phone,String password) {
        this.firstName = firstName1;
        this.lastName = lastName;
        this.phone = phone;
        this.password = password;

    }

//    public User(String firstName,String password){
//
//    }


    //Getters and setters
    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String name) {
        firstName = name;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }
}