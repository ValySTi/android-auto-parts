package sicktrending.com.sicktrending;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.support.design.widget.CollapsingToolbarLayout;
import android.support.design.widget.NavigationView;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.andremion.counterfab.CounterFab;
import com.cepheuen.elegantnumberbutton.view.ElegantNumberButton;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.squareup.picasso.Picasso;

import io.paperdb.Paper;
import sicktrending.com.sicktrending.Common.Common;
import sicktrending.com.sicktrending.Database.Database;
import sicktrending.com.sicktrending.Model.Order;
import sicktrending.com.sicktrending.Model.Piese;
import uk.co.chrisjenx.calligraphy.CalligraphyConfig;
import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;

public class PieseDetail extends AppCompatActivity implements NavigationView.OnNavigationItemSelectedListener{

    TextView piese_name, piese_price, piese_description;
    ImageView piese_image;
    CollapsingToolbarLayout collapsingToolbarLayout;
    CounterFab btnCart;
    ElegantNumberButton numberButton;

    String pieseDetailId ="";

    FirebaseDatabase database;
    DatabaseReference piese;
    TextView textPieseDetails;
    Piese currentPiese;
    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase));
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        CalligraphyConfig.initDefault(new CalligraphyConfig.Builder()
                .setDefaultFontPath("font/caviar.ttf")
                .setFontAttrId(R.attr.fontPath)
                .build());
        setContentView(R.layout.activity_piese_detail);

        //Firebase
        database = FirebaseDatabase.getInstance();
        piese = database.getReference("Piese");

        //Init view
        numberButton = (ElegantNumberButton)findViewById(R.id.number_button);
        btnCart = (CounterFab) findViewById(R.id.btnCart);

        btnCart.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                new Database(getBaseContext()).addToCart(new Order(
                        pieseDetailId,
                        currentPiese.getName(),
                        numberButton.getNumber(),
                        currentPiese.getPrice(),
                        currentPiese.getDiscount()

                ));
                addOneItem();
                Toast.makeText(PieseDetail.this,"Produs adaugat in cos" , Toast.LENGTH_SHORT).show();
            }
        });


        btnCart.setCount(new Database(this).getCountCart());

        piese_description = (TextView)findViewById(R.id.piese_description);
        piese_name = (TextView)findViewById(R.id.piese_name);
        piese_price = (TextView)findViewById(R.id.piese_price);
        piese_image = (ImageView)findViewById(R.id.img_piese);

        collapsingToolbarLayout = (CollapsingToolbarLayout)findViewById(R.id.collapsing);
        collapsingToolbarLayout.setExpandedTitleTextAppearance(R.style.ExpandedAppbar);
        collapsingToolbarLayout.setCollapsedTitleTextAppearance(R.style.CollapsedAppbar);

        //Get piese id from intent
        if(getIntent() !=null)
            pieseDetailId = getIntent().getStringExtra("PieseDetailId");
        if(!pieseDetailId.isEmpty())
        {   if (Common.isConnectedToInternet(getBaseContext()))
            getDetailPiese(pieseDetailId);
            else
        {
            Toast.makeText(PieseDetail.this , "Nu exista conexiune la internet!",Toast.LENGTH_SHORT).show();
            return;
        }
        }
        NavigationView navigationView = (NavigationView) findViewById(R.id.navigation_view_piesedetails);
        navigationView.setNavigationItemSelectedListener(this);

        //Set Name for user
        View headerView = navigationView.getHeaderView(0);
        textPieseDetails = (TextView)headerView.findViewById(R.id.txtFullName);
        textPieseDetails.setText(Common.currentUser.getLastName()+ " "+ Common.currentUser.getFirstName());
    }

    private void addOneItem(){
        btnCart.setCount(new Database(this).getCountCart());
    }

    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        int id = item.getItemId();

        if (id == R.id.nav_cart) {
            Intent cartIntent = new Intent(PieseDetail.this, Cart.class);
            startActivity(cartIntent);
            onBackPressed();
        }else if(id == R.id.nav_home){
            Intent homeIntent = new Intent(PieseDetail.this,Home.class);
            startActivity(homeIntent);
            onBackPressed();
        }else if (id == R.id.nav_log_out) {

            //Delete Remember user & pass
            Paper.book().destroy();

            Intent signIn = new Intent(PieseDetail.this, MainActivity.class);
            signIn.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
            startActivity(signIn);

        }
        else if(id == R.id.nav_about_us){
            CalligraphyConfig.initDefault(new CalligraphyConfig.Builder()
                    .setDefaultFontPath("font/caviar.ttf")
                    .setFontAttrId(R.attr.fontPath)
                    .build());
            AlertDialog.Builder alertDialog = new AlertDialog.Builder(PieseDetail.this);
            alertDialog.setTitle("Despre noi");
            alertDialog.setMessage("Ne gasiti pe Strada Mihai Viteazul 42, Brasov 500187\n" +
                    "Telefon: 031 130 0429\n" + "Email: vanzari@carparts.com\n");

            alertDialog.setNegativeButton("OK", new DialogInterface.OnClickListener() {

                @Override
                public void onClick(DialogInterface dialogInterface, int i) {
                    dialogInterface.dismiss();
                }
            });

            alertDialog.show();

        }
        else if (id == R.id.nav_user_settings){
            Intent userSettingsIntent = new Intent(PieseDetail.this,UserSettings.class);
            startActivity(userSettingsIntent);

        }

        return true;
    }

    private void getDetailPiese(final String pieseId) {
        piese.child(pieseId).addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
               currentPiese = dataSnapshot.getValue(Piese.class);

                //Set Image
                Picasso.with(getBaseContext()).load(currentPiese.getImage()).into(piese_image);



                piese_price.setText(currentPiese.getPrice());

                piese_name.setText(currentPiese.getName());

                piese_description.setText(currentPiese.getDescription());
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });
    }



}
