 package sicktrending.com.sicktrending;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.andremion.counterfab.CounterFab;
import com.firebase.ui.database.FirebaseRecyclerAdapter;
import com.firebase.ui.database.FirebaseRecyclerOptions;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.Query;
import com.google.firebase.database.ValueEventListener;
import com.mancj.materialsearchbar.MaterialSearchBar;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;

import io.paperdb.Paper;
import sicktrending.com.sicktrending.Common.Common;
import sicktrending.com.sicktrending.Database.Database;
import sicktrending.com.sicktrending.Interface.ItemClickListener;
import sicktrending.com.sicktrending.Model.Order;
import sicktrending.com.sicktrending.Model.Piese;
import sicktrending.com.sicktrending.ViewHolder.PieseViewHolder;
import uk.co.chrisjenx.calligraphy.CalligraphyConfig;
import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;

 public class PieseList extends AppCompatActivity implements NavigationView.OnNavigationItemSelectedListener {

    RecyclerView recyclerView;
    RecyclerView.LayoutManager layoutManager;

    FirebaseDatabase database;
    DatabaseReference pieseList;
    String pieseId ="";
    CounterFab fab2;
    TextView textPieseList;

    FirebaseRecyclerAdapter<Piese, PieseViewHolder> adapter;

    //Search Funct
    FirebaseRecyclerAdapter<Piese, PieseViewHolder> searchAdapter;
    List<String> suggestList = new ArrayList<>();
    MaterialSearchBar materialSearchBar;

    SwipeRefreshLayout swipeRefreshLayout;
     @Override
     protected void attachBaseContext(Context newBase) {
         super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase));
     }


     @SuppressLint("ResourceAsColor")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        CalligraphyConfig.initDefault(new CalligraphyConfig.Builder()
                .setDefaultFontPath("font/caviar.ttf")
                .setFontAttrId(R.attr.fontPath)
                .build());
        setContentView(R.layout.activity_piese_list);



        //Firebase
        database = FirebaseDatabase.getInstance();
        pieseList = database.getReference("Piese");

        swipeRefreshLayout = (SwipeRefreshLayout)findViewById(R.id.swipe_layout);
        swipeRefreshLayout.setColorSchemeColors(R.color.colorPrimary,
                android.R.color.holo_blue_dark,
                android.R.color.holo_orange_dark,
                android.R.color.holo_red_dark
        );
        swipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                //Get Intent here
                if(getIntent() !=null)
                    pieseId = getIntent().getStringExtra("PieseId");
                if(!pieseId.isEmpty() && pieseId != null)
                {
                    if(Common.isConnectedToInternet(getBaseContext()))
                        loadListPiese(pieseId);

                    else {
                        Toast.makeText(PieseList.this , "Nu exista conexiune la internet!",Toast.LENGTH_SHORT).show();
                        return;
                    }
                }
            }
        });
        swipeRefreshLayout.post(new Runnable() {
            @Override
            public void run() {
                //Get Intent here
                if(getIntent() !=null)
                    pieseId = getIntent().getStringExtra("PieseId");
                if(!pieseId.isEmpty() && pieseId != null)
                {
                    if(Common.isConnectedToInternet(getBaseContext()))
                        loadListPiese(pieseId);
                    else {
                        Toast.makeText(PieseList.this , "Nu exista conexiune la internet!",Toast.LENGTH_SHORT).show();
                        return;
                    }
                }

                //Search
                materialSearchBar = (MaterialSearchBar)findViewById(R.id.searchBar);
                materialSearchBar.setHint("Cauta piesa");
                loadSuggest(); //Suggest form Firebase

                materialSearchBar.setCardViewElevation(10);
                materialSearchBar.addTextChangeListener(new TextWatcher() {
                    @Override
                    public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

                    }

                    @Override
                    public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                        //When user types text change suggest list
                        List<String> suggest = new ArrayList<String>();
                        for(String search:suggestList)
                        {
                            if(search.toLowerCase().contains(materialSearchBar.getText().toLowerCase()))
                                suggest.add(search);
                        }
                        materialSearchBar.setLastSuggestions(suggest);

                    }

                    @Override
                    public void afterTextChanged(Editable editable) {

                    }
                });
                materialSearchBar.setOnSearchActionListener(new MaterialSearchBar.OnSearchActionListener() {
                    @Override
                    public void onSearchStateChanged(boolean enabled) {
                        //When Search Bar is close
                        //restore original suggest adapter
                        if(!enabled)
                            recyclerView.setAdapter(adapter);
                    }

                    @Override
                    public void onSearchConfirmed(CharSequence text) {
                        //When search finish
                        //Show result
                        startSearch(text);

                    }

                    @Override
                    public void onButtonClicked(int buttonCode) {

                    }
                });

            }
        });



        recyclerView = (RecyclerView)findViewById(R.id.recycler_pieselist);
        recyclerView.setHasFixedSize(true);
        //layoutManager = new LinearLayoutManager(this);
        //recycler_piese.setLayoutManager(layoutManager);
        recyclerView.setLayoutManager(new GridLayoutManager(this , 2));

         fab2 = (CounterFab) findViewById(R.id.fab2);
         fab2.setOnClickListener(new View.OnClickListener() {
             @Override
             public void onClick(View view) {
                 Intent cartIntent = new Intent(PieseList.this,Cart.class);
                 startActivity(cartIntent);
             }
         });
         fab2.setCount(new Database(this).getCountCart());

         NavigationView navigationView = (NavigationView) findViewById(R.id.navigation_view_pieselist);
         navigationView.setNavigationItemSelectedListener(this);

         //Set Name for user
         View headerView = navigationView.getHeaderView(0);
         textPieseList = (TextView)headerView.findViewById(R.id.txtFullName);
         textPieseList.setText(Common.currentUser.getLastName()+ " "+ Common.currentUser.getFirstName());





     }

     @Override
     protected void onResume() {
         super.onResume();
         fab2.setCount(new Database(this).getCountCart());
         if(adapter != null)
             adapter.startListening();
     }

     @Override
     protected void onPause() {
         super.onPause();
         fab2.setCount(new Database(this).getCountCart());
     }

     private void startSearch(CharSequence text) {
         Query searchByName = pieseList.orderByChild("Name").equalTo(text.toString());

         FirebaseRecyclerOptions<Piese> pieseOptions = new FirebaseRecyclerOptions.Builder<Piese>()
                 .setQuery(searchByName, Piese.class)
                 .build();
        searchAdapter =  new FirebaseRecyclerAdapter<Piese, PieseViewHolder>(pieseOptions) {
            @Override
            protected void onBindViewHolder(@NonNull PieseViewHolder viewHolder, int position, @NonNull Piese model) {
                viewHolder.piese_name.setText(model.getName());
                Picasso.with(getBaseContext()).load(model.getImage()).into(viewHolder.piese_image);

                final Piese local = model;
                viewHolder.setItemClickListener(new ItemClickListener() {
                    @Override
                    public void onClick(View view, int position, boolean isLongClick) {
                        //Start new Activity
                        Intent pieseDetail = new Intent(PieseList.this, PieseDetail.class);
                        pieseDetail.putExtra("PieseDetailId",searchAdapter.getRef(position).getKey());
                        startActivity(pieseDetail);
                    }
                });


            }

            @Override
            public PieseViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
                View itemView = LayoutInflater.from(parent.getContext())
                        .inflate(R.layout.piese_item,parent,false);
                return new PieseViewHolder(itemView);            }
        };

                searchAdapter.startListening();
                recyclerView.setAdapter(searchAdapter);
     }

     private void addPartItem(){
         fab2.setCount(new Database(this).getCountCart());
     }

     private void loadSuggest() {
        pieseList.orderByChild("PieseId").equalTo(pieseId).addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                for(DataSnapshot postSnapshot:dataSnapshot.getChildren())
                {
                    Piese item = postSnapshot.getValue(Piese.class);
                    suggestList.add(item.getName()); // Add name of piese
                }

                materialSearchBar.setLastSuggestions(suggestList);
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });
     }

     private void loadListPiese(String pieseId) {

         Query searchByName = pieseList.orderByChild("PieseId").equalTo(pieseId);

         FirebaseRecyclerOptions<Piese> pieseOptions = new FirebaseRecyclerOptions.Builder<Piese>()
                 .setQuery(searchByName, Piese.class)
                 .build();

        adapter = new FirebaseRecyclerAdapter<Piese, PieseViewHolder>(pieseOptions) {
            @Override
            protected void onBindViewHolder(@NonNull PieseViewHolder viewHolder, final int position, @NonNull final Piese model) {

                viewHolder.piese_name.setText(model.getName());
                viewHolder.piese_price.setText(String.format("€ %s",model.getPrice().toString()));

                Picasso.with(getBaseContext()).load(model.getImage()).into(viewHolder.piese_image);

                //quick cart
                viewHolder.quick_cart.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        new Database(getBaseContext()).addToCart(new Order(
                                adapter.getRef(position).getKey(),
                                model.getName(),
                                "1",
                                model.getPrice(),
                                model.getDiscount()
                        ));
                        addPartItem();
                        Toast.makeText(PieseList.this,"Produs adaugat in cos!" , Toast.LENGTH_SHORT).show();

                    }
                });

                final Piese local = model;
                viewHolder.setItemClickListener(new ItemClickListener() {
                    @Override
                    public void onClick(View view, int position, boolean isLongClick) {
                        //Start new Activity
                        Intent pieseDetail = new Intent(PieseList.this, PieseDetail.class);
                        pieseDetail.putExtra("PieseDetailId",adapter.getRef(position).getKey());
                        startActivity(pieseDetail);
                    }
                });



            }

            @Override
            public PieseViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
                View itemView = LayoutInflater.from(parent.getContext())
                        .inflate(R.layout.piese_item,parent,false);
                return new PieseViewHolder(itemView);
        }
        };

        adapter.startListening();

         recyclerView.setAdapter(adapter); //adapter for Recycler View is Search result
         swipeRefreshLayout.setRefreshing(false);

     }



     @Override
     public void onBackPressed() {
         DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_pieselist);
         if (drawer.isDrawerOpen(GravityCompat.START)) {
             drawer.closeDrawer(GravityCompat.START);
         } else {
             super.onBackPressed();
         }
     }

     @Override
     public boolean onNavigationItemSelected(MenuItem item) {
         int id = item.getItemId();

         if (id == R.id.nav_cart) {
             Intent cartIntent = new Intent(PieseList.this, Cart.class);
             startActivity(cartIntent);
             super.onBackPressed();
         }else if(id == R.id.nav_home){
             Intent homeIntent = new Intent(PieseList.this,Home.class);
             startActivity(homeIntent);
             onBackPressed();
         }else if (id == R.id.nav_log_out) {

             //Delete Remember user & pass
             Paper.book().destroy();

             Intent signIn = new Intent(PieseList.this, MainActivity.class);
             signIn.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
             startActivity(signIn);

         }
         else if(id == R.id.nav_about_us){
             CalligraphyConfig.initDefault(new CalligraphyConfig.Builder()
                     .setDefaultFontPath("font/caviar.ttf")
                     .setFontAttrId(R.attr.fontPath)
                     .build());
             AlertDialog.Builder alertDialog = new AlertDialog.Builder(PieseList.this);
             alertDialog.setTitle("Despre noi");
             alertDialog.setMessage("Ne gasiti pe Strada Mihai Viteazul 42, Brasov 500187\n" +
                     "Telefon: 031 130 0429\n" + "Email: vanzari@carparts.com\n");

             alertDialog.setNegativeButton("OK", new DialogInterface.OnClickListener() {

                 @Override
                 public void onClick(DialogInterface dialogInterface, int i) {
                     dialogInterface.dismiss();
                 }
             });

             alertDialog.show();

         }
         else if (id == R.id.nav_user_settings){
             Intent userSettingsIntent = new Intent(PieseList.this,UserSettings.class);
             startActivity(userSettingsIntent);

         }
         return true;
     }

     @Override
     protected void onStop() {
         super.onStop();
         if(adapter != null)
             adapter.stopListening();
         if(searchAdapter != null)
         searchAdapter.stopListening();
     }
 }
