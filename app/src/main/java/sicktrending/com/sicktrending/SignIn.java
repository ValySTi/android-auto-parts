package sicktrending.com.sicktrending;

import android.app.ProgressDialog;
import android.content.ActivityNotFoundException;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Handler;
import android.os.Looper;
import android.os.Message;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.rengwuxian.materialedittext.MaterialEditText;

import io.paperdb.Paper;
import sicktrending.com.sicktrending.Common.Common;
import sicktrending.com.sicktrending.Model.User;
import uk.co.chrisjenx.calligraphy.CalligraphyConfig;
import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;

public class SignIn extends AppCompatActivity {
    MaterialEditText edtEmail, edtPassword;
    Button btnSignIn;
    //com.rey.material.widget.CheckBox ckbRemember;
    TextView txtForgotPass;

    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase));
    }


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        CalligraphyConfig.initDefault(new CalligraphyConfig.Builder()
                .setDefaultFontPath("font/caviar.ttf")
                .setFontAttrId(R.attr.fontPath)
                .build());
        setContentView(R.layout.activity_sign_in);

        edtPassword = (MaterialEditText) findViewById(R.id.edtPassword);
        edtEmail = (MaterialEditText) findViewById(R.id.edtLoginEmail);
        btnSignIn = (Button) findViewById(R.id.btnSignIn);
//        ckbRemember = (com.rey.material.widget.CheckBox) findViewById(R.id.ckbRemember);
        txtForgotPass = (TextView) findViewById(R.id.txtForgotPass);

        //Init Paper
        Paper.init(this);

        //Init Firebase
        final FirebaseDatabase database = FirebaseDatabase.getInstance();
        final DatabaseReference table_user = database.getReference("User");

        txtForgotPass.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (edtEmail.getText().toString().equals("") || edtPassword.getText().toString().equals("")) {
                    Toast.makeText(SignIn.this, "Inserati toate campurile", Toast.LENGTH_SHORT).show();

                } else {
                    table_user.addListenerForSingleValueEvent(new ValueEventListener() {
                        @Override
                        public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                            final DataSnapshot dataSnapshot1 = dataSnapshot.child(edtEmail.getText().toString());
                            final DataSnapshot dataSnapshot2 = dataSnapshot1.child("password");
                            new Thread(new Runnable() {
                                @Override
                                public void run() {
                                    try {
                                        String emailWithPoint = dataSnapshot1.getKey();
                                        emailWithPoint=emailWithPoint.replace(",",".");
                                        GmailSender sender = new GmailSender("coolcarparts01@gmail.com", "12345test");
                                        sender.sendMail("Solicitare email si parola", "Numele tau de utilizator este " + dataSnapshot1.getKey() + " iar parola " +
                                                        "utilizatorului este " + dataSnapshot2.getValue(),

                                                "coolcarparts01@gmail.com",

                                                emailWithPoint);


                                    } catch (Exception ex) {
                                        Toast.makeText(getApplicationContext(), "Error", Toast.LENGTH_LONG).show();
                                    }
                                }
                            }).start();
                        }

                        @Override
                        public void onCancelled(@NonNull DatabaseError databaseError) {

                        }
                    });

                    Toast.makeText(SignIn.this, "Email trimis catre " + edtEmail.getText().toString(), Toast.LENGTH_SHORT).show();
                }
            }
        });

        btnSignIn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if (Common.isConnectedToInternet(getBaseContext())) {

                    final ProgressDialog mDialog = new ProgressDialog(SignIn.this);
                    mDialog.setMessage("Va rugam asteptati...");
                    mDialog.show();

                   table_user.addListenerForSingleValueEvent((new ValueEventListener() {


                        @Override
                        public void onDataChange(DataSnapshot dataSnapshot) {
                            //Check if user exists in database
                            if (dataSnapshot.child(edtEmail.getText().toString()).exists()) {
                                //Get User Info
                                mDialog.dismiss();
                                User user = dataSnapshot.child(edtEmail.getText().toString()).getValue(User.class);
                                user.setEmail(edtEmail.getText().toString()); // set Email
                                if (user.getPassword().equals(edtPassword.getText().toString())) {
                                    Intent homeIntent = new Intent(SignIn.this, Home.class);
                                    Common.currentUser = user;
                                    startActivity(homeIntent);
                                    finish();
                                } else {
                                    Toast.makeText(SignIn.this, "Parola gresita !", Toast.LENGTH_SHORT).show();
                                }
                            } else {
                                mDialog.dismiss();
                                Toast.makeText(SignIn.this, "Utilizatorul nu exista!", Toast.LENGTH_SHORT).show();
                            }
                        }

                        @Override
                        public void onCancelled(DatabaseError databaseError) {

                        }
                    }));
                } else {
                    Toast.makeText(SignIn.this, "Nu exista conexiune la internet!", Toast.LENGTH_SHORT).show();
                    return;
                }
            }

        });
    }


}
