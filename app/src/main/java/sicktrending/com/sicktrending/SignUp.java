package sicktrending.com.sicktrending;

import android.app.ProgressDialog;
import android.content.Context;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.rengwuxian.materialedittext.MaterialEditText;

import sicktrending.com.sicktrending.Common.Common;
import sicktrending.com.sicktrending.Model.User;
import uk.co.chrisjenx.calligraphy.CalligraphyConfig;
import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;

public class SignUp extends AppCompatActivity {

    MaterialEditText edtPhone, edtFirstName, edtPassword, edtEmail, edtLastName;
    Button btnSignUp;

    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase));
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        CalligraphyConfig.initDefault(new CalligraphyConfig.Builder()
                .setDefaultFontPath("font/caviar.ttf")
                .setFontAttrId(R.attr.fontPath)
                .build());
        setContentView(R.layout.activity_sign_up);

        edtFirstName = (MaterialEditText) findViewById(R.id.edtFirstName);
        edtLastName = (MaterialEditText) findViewById(R.id.edtLastName);
        edtEmail = (MaterialEditText) findViewById(R.id.edtEmail);
        edtPhone = (MaterialEditText) findViewById(R.id.edtRegisterPhone);
        edtPassword = (MaterialEditText) findViewById(R.id.edtRegisterPassword);


        btnSignUp = (Button) findViewById(R.id.btnSignUp);

        //Init Firebase
        final FirebaseDatabase database = FirebaseDatabase.getInstance();
        final DatabaseReference table_user = database.getReference("User");

        btnSignUp.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (Common.isConnectedToInternet(getBaseContext())) {
                    final ProgressDialog mDialog = new ProgressDialog(SignUp.this);
                    mDialog.setMessage("Va rugam asteptati...");
                    mDialog.show();


                    table_user.addListenerForSingleValueEvent((new ValueEventListener() {

                        @Override
                        public void onDataChange(DataSnapshot dataSnapshot) {
                            //Check if already user phone
                            if (dataSnapshot.child(edtEmail.getText().toString()).exists()) {
                                mDialog.dismiss();
                                Toast.makeText(SignUp.this, "Email-ul deja exista !", Toast.LENGTH_SHORT).show();
                            } else {
                                mDialog.dismiss();
                                User user = new User(edtFirstName.getText().toString(), edtLastName.getText().toString(), edtEmail.getText().toString(),
                                        edtPhone.getText().toString(), edtPassword.getText().toString());
                                table_user.child(edtEmail.getText().toString()).setValue(user);
                                Toast.makeText(SignUp.this, "Contul a foste creat cu succes !", Toast.LENGTH_SHORT).show();
                                finish();
                            }
                        }

                        @Override
                        public void onCancelled(DatabaseError databaseError) {

                        }
                    }));
                } else {
                    Toast.makeText(SignUp.this, "Nu exista conexiune la internet!", Toast.LENGTH_SHORT).show();
                    return;
                }

            }
        });
    }
}
