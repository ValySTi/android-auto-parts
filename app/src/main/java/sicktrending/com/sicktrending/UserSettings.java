package sicktrending.com.sicktrending;


import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.rengwuxian.materialedittext.MaterialEditText;

import io.paperdb.Paper;
import sicktrending.com.sicktrending.Common.Common;
import uk.co.chrisjenx.calligraphy.CalligraphyConfig;

public class UserSettings extends AppCompatActivity {

    MaterialEditText edtOldPassword, edtNewPassword;
    Button btnChangePassword;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        CalligraphyConfig.initDefault(new CalligraphyConfig.Builder()
                .setDefaultFontPath("font/caviar.ttf")
                .setFontAttrId(R.attr.fontPath)
                .build());
        setContentView(R.layout.activity_user_settings);

        edtOldPassword = (MaterialEditText) findViewById(R.id.oldPassword);
        edtNewPassword = (MaterialEditText) findViewById(R.id.newPassword);
        btnChangePassword = (Button) findViewById(R.id.btnChangePassword);
        btnChangePassword.setOnClickListener(new View.OnClickListener() {

        //Firebase
        final FirebaseDatabase firebaseDatabase = FirebaseDatabase.getInstance();
        final DatabaseReference databaseReference = firebaseDatabase.getReference("User");



            @Override
            public void onClick(View view) {

                final ProgressDialog mDialog = new ProgressDialog(UserSettings.this);
                mDialog.setMessage("Va rugam asteptati...");
                mDialog.show();

                if (!edtOldPassword.getText().toString().equals(Common.currentUser.getPassword())) {
                    mDialog.dismiss();
                    Toast.makeText(UserSettings.this, "Parola veche a fost introdusa gresit!"
                            , Toast.LENGTH_SHORT).show();
                } else if (edtOldPassword.getText().toString().equals(Common.currentUser.getPassword())) {
                    mDialog.dismiss();
                    Toast.makeText(UserSettings.this, "Parola veche a fost introdusa corect!", Toast.LENGTH_SHORT).show();
                    databaseReference.child(Common.currentUser.getEmail()).child("password").setValue(edtNewPassword.getText().toString());
                    Toast.makeText(UserSettings.this, "Parola schimbata cu succes!", Toast.LENGTH_SHORT).show();

                    Paper.book().destroy();
                    Intent logOut = new Intent(UserSettings.this, MainActivity.class);
                    logOut.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                    startActivity(logOut);
                } else {
                    mDialog.dismiss();
                    Toast.makeText(UserSettings.this, "A aparut o eroare!", Toast.LENGTH_SHORT).show();
                }

            }
        });
    }


}
