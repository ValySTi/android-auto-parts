package sicktrending.com.sicktrending.ViewHolder;


import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import sicktrending.com.sicktrending.Interface.ItemClickListener;
import sicktrending.com.sicktrending.R;

public class CategoriiViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener{

    public TextView txtCategorieName;
    public ImageView imageView;

    private ItemClickListener itemClickListener;

    public CategoriiViewHolder(View itemView) {
        super(itemView);

        txtCategorieName = (TextView)itemView.findViewById(R.id.categorie_name);
        imageView = (ImageView)itemView.findViewById(R.id.categorie_image);

        itemView.setOnClickListener(this);
    }

    public void setItemClickListener(ItemClickListener itemClickListener) {
        this.itemClickListener = itemClickListener;
    }

    @Override
    public void onClick(View view) {
        itemClickListener.onClick(view,getAdapterPosition(),false);

    }
}
