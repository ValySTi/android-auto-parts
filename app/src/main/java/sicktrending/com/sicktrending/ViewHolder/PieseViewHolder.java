package sicktrending.com.sicktrending.ViewHolder;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import sicktrending.com.sicktrending.Interface.ItemClickListener;
import sicktrending.com.sicktrending.R;


public class PieseViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener{
    public TextView piese_name, piese_price;
    public ImageView piese_image, quick_cart;

    private ItemClickListener itemClickListener;

    public void setItemClickListener(ItemClickListener itemClickListener) {
        this.itemClickListener = itemClickListener;

    }

    public PieseViewHolder(View itemView) {
        super(itemView);
        piese_name = (TextView)itemView.findViewById(R.id.piese_name);
        piese_image = (ImageView)itemView.findViewById(R.id.piese_image);
        quick_cart = (ImageView)itemView.findViewById(R.id.btn_quick_cart);
        piese_price = (TextView)itemView.findViewById(R.id.piese_price);

        itemView.setOnClickListener(this);
    }

    @Override
    public void onClick(View view) {
        itemClickListener.onClick(view,getAdapterPosition(),false);

    }
}
